$(document).ready(function(){

	$(".has-sub ul li").hide();

	$(".has-sub").mouseenter(function(){
		$(".has-sub ul li").slideDown();
	})
	.mouseleave(function(){
		$(".has-sub ul li").slideUp();
	});

});